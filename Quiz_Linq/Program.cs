﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_Linq
{

    /*Quiz Linq - Semillero Desarrollo .Net Heishon
     Elaborado por: Saul Machado Gonzalez
     email: ingmachado07@gmail.com
     */
    class Program
    {
        static void Main(string[] args)
        {

            List<Customer> listaCustomers = new List<Customer>();
            List<Order> listaOrders = new List<Order>();
            List<Product> listaProduct = new List<Product>();
            int opcion;

            SqlConnection db = new SqlConnection("Data Source=SAUL-PC;Initial Catalog=Northwind;Integrated Security=True"); ;
            db.Open();
            
            //Sentencia 1: Consulta los Customers/Clientes
            using (SqlCommand cmd = db.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CustomerId, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country " +
                                   "FROM Customers WHERE 1=1";

                SqlDataReader datos = cmd.ExecuteReader();

                while (datos.Read())
                {
                    //Creamos un objeto customer
                    Customer customer = new Customer();
                    customer.CustomerId = datos["CustomerId"].ToString();
                    customer.CompanyName = datos["CompanyName"].ToString();
                    customer.ContactName = datos["ContactName"].ToString();
                    customer.ContactTitle = datos["ContactTitle"].ToString();
                    customer.Addres = datos["Address"].ToString();
                    customer.City = datos["City"].ToString();
                    customer.Region = datos["Region"].ToString();
                    customer.PostalCode = datos["PostalCode"].ToString();
                    customer.Country = datos["Country"].ToString();

                    //Agregamos el objeto a la lista
                    listaCustomers.Add(customer);
                }
                
                datos.Close();
            }

            //Sentencia 2: Consulta las Orders/Pedidos
            using (SqlCommand cmd2 = db.CreateCommand())
            {
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = "SELECT OrderID, CustomerID, EmployeeID, OrderDate, RequiredDate, ShippedDate, ShipVia," +
                                   "Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry " +
                                   "FROM Orders WHERE 1=1";

                SqlDataReader drOrders = cmd2.ExecuteReader();

                while (drOrders.Read())
                {
                    //Creamos un objeto customer
                    Order objOrder = new Order();

                    objOrder.OrderId = Convert.ToInt32(drOrders["OrderID"]);
                    objOrder.CustomerId = Convert.ToString(drOrders["CustomerID"]);
                    objOrder.EmployeeId = Convert.ToInt32(drOrders["EmployeeID"]);
                    objOrder.OrderDate = Convert.ToDateTime(drOrders["OrderDate"]);
                    objOrder.RequiredDate = Convert.ToDateTime(drOrders["RequiredDate"]);
                    objOrder.ShippedDate = (DateTime?)(drOrders.IsDBNull( drOrders.GetOrdinal("ShippedDate")) ? null : drOrders["ShippedDate"]);
                    objOrder.ShipVia = Convert.ToInt32(drOrders["ShipVia"]);
                    objOrder.Freight = Convert.ToDecimal(drOrders["Freight"]);
                    objOrder.ShipName = Convert.ToString(drOrders["ShipName"]);
                    objOrder.ShipAddres = Convert.ToString(drOrders["ShipAddress"]);
                    objOrder.ShipCity = Convert.ToString(drOrders["ShipCity"]);
                    objOrder.ShipRegion = Convert.ToString(drOrders["ShipRegion"]);
                    objOrder.ShipPostalCode = Convert.ToString(drOrders["ShipPostalCode"]);
                    objOrder.ShipCountry = Convert.ToString(drOrders["ShipCountry"]);

                    //Agregamos el objeto a la lista de pedidos
                    listaOrders.Add(objOrder);
                }

                drOrders.Close();
            }

            //Sentencia 3: Consulta los Productos
            using (SqlCommand cmd3 = db.CreateCommand())
            {
                cmd3.CommandType = CommandType.Text;
                cmd3.CommandText = "SELECT ProductID, ProductName, SupplierID, CategoryID, QuantityPerUnit," +
                                   "UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued " +
                                   "FROM Products WHERE 1=1";

                SqlDataReader drProduct = cmd3.ExecuteReader();

                while (drProduct.Read())
                {
                    //Creamos un objeto customer
                    Product objProduct = new Product();

                    objProduct.ProductId = Convert.ToInt32(drProduct["ProductID"]);
                    objProduct.ProductName = Convert.ToString(drProduct["ProductName"]);
                    objProduct.SupplierId = Convert.ToInt32(drProduct["SupplierID"]);
                    objProduct.CategoryId = Convert.ToInt32(drProduct["CategoryID"]);
                    objProduct.QuantityPerUnit = Convert.ToString(drProduct["QuantityPerUnit"]);
                    objProduct.UnitPrice = Convert.ToDecimal(drProduct["UnitPrice"]);
                    objProduct.UnitsInStock = Convert.ToInt32(drProduct["UnitsInStock"]);
                    objProduct.UnitsOnOrder = Convert.ToInt32(drProduct["UnitsOnOrder"]);
                    objProduct.ReorderLevel = Convert.ToInt32(drProduct["ReorderLevel"]);
                    objProduct.Discontinued = Convert.ToInt32(drProduct["Discontinued"]);

                    //Agregamos el objeto a la lista de pedidos
                    listaProduct.Add(objProduct);
                }

                drProduct.Close();
            }
            
            do
            {
                Console.WriteLine("-------------BIENVENIDO------------");
                Console.WriteLine("Seleccione una opcion*");
                Console.WriteLine("1) Mostrar datos de la tabla Customers");
                Console.WriteLine("2) Mostrar Clientes con pais Germany");
                Console.WriteLine("3) Mostrar numero de Clientes registrados");
                Console.WriteLine("4) Mostrar ciudades registradas en DB");
                Console.WriteLine("5) Mostrar clientes ordenados descendente");
                Console.WriteLine("6) Mostrar pedidos que contengas <OM> en CustomerID");
                Console.WriteLine("7) Mostrar todos los productos");
                Console.WriteLine("8) Mostrar suma de precios unitarios de productos agrupados por categoria");
                Console.WriteLine("9) Mostrar productos cuyos precio unitario supere el promedio de precios");
                Console.WriteLine("0) Salir");
                opcion = Convert.ToInt32(Console.ReadLine());

                switch (opcion)
                {
                    case 1:
                        {
                            //Mostramos los datos de customerss
                            Console.WriteLine("1) Datos de la tabla customers");
                            Console.WriteLine("------------------------------------");
                            foreach (Customer c in listaCustomers)
                            {
                                Console.WriteLine(c.ContactName + " - " + c.ContactTitle + " - " + c.Country);
                            }
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 2:
                        {
                            //2.a
                            var customerGermany = from c in listaCustomers
                                                  where c.Country == "Germany"
                                                  select c;

                            //Mostramos los usuarios con pais Alemania
                            Console.WriteLine("2) Usuarios de Alemania: ");
                            foreach (Customer c in customerGermany)
                            {
                                Console.WriteLine(c.ContactName);
                            }
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 3:
                        {

                            //2.b
                            int cantidadCustomer = listaCustomers.Count();
                            Console.WriteLine("\n");
                            Console.WriteLine("3) El numero de clientes registrados en DB es: {0}", cantidadCustomer);

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 4:
                        {

                            //2.c
                            var cities = listaCustomers.Select(elemento => elemento.City).Distinct();

                            //mostramos las ciudades registradas en base de datos
                            Console.WriteLine("\n");
                            Console.WriteLine("4) Ciudades registradas en DB: ");
                            foreach (string city in cities)
                            {
                                Console.WriteLine(city);
                            }

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 5:
                        {

                            //2.d
                            var customerOrderContactName = from c in listaCustomers
                                                           orderby c.ContactName descending
                                                           select c;
                            Console.WriteLine("\n");
                            Console.WriteLine("5) Lista de usuarios ordenada por nombre descendente");
                            foreach (Customer customer1 in customerOrderContactName)
                            {
                                Console.WriteLine(customer1.ContactName);
                            }

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 6:
                        {

                            //2.e obtenemos los pedidos que tienen OM en CustomerID
                            var ordersFilter = listaOrders.Where(elemento => elemento.CustomerId.Contains("OM"))
                                                           .Select(elemento => new { elemento.CustomerId, elemento.ShipName })
                                                           .Distinct();

                            Console.WriteLine("\n");
                            Console.WriteLine("6) Lista de pedidos que contienen <OM> en CustomerID");
                            foreach (var customer in ordersFilter)
                            {
                                Console.WriteLine(customer.CustomerId + " - " + customer.ShipName);
                            }

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 7:
                        {
                            //Punto 3
                            Console.WriteLine("\n");
                            Console.WriteLine("7) Lista de productos");
                            foreach (Product product in listaProduct)
                            {
                                Console.WriteLine(product.ProductId + " - " + product.ProductName + " - stock: " + product.UnitsInStock);
                            }

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 8:
                        {
                            //Punto 4a
                            var productsOnCategory = listaProduct.GroupBy(elemento => elemento.CategoryId);

                            var sumaPriceOnCategory = from p in productsOnCategory
                                                      select new
                                                      {
                                                          id = p.Key,
                                                          suma = p.Sum(e => e.UnitPrice)
                                                      };

                            Console.WriteLine("\n");
                            Console.WriteLine("8) La suma los precios unitarios por categoria es: ");
                            foreach (var objSuma in sumaPriceOnCategory)
                            {
                                Console.WriteLine(objSuma.id + " => " + objSuma.suma);
                            }

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 9:
                        {
                            //Punto 4b
                            decimal promedio = listaProduct.Average(elemento => elemento.UnitPrice);
                            Console.WriteLine("\n");
                            Console.WriteLine("8) Lista de productos por encima del promedio");
                            Console.WriteLine("Precio Promedio = {0}", promedio);
                            foreach (Product product in listaProduct)
                            {
                                if (product.UnitPrice > promedio)
                                {
                                    Console.WriteLine(product.ProductId + " - " + product.ProductName + " - stock: " + product.UnitsInStock + " - precio unitario: " + product.UnitPrice);
                                }
                            }

                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Bye");
                            break;
                        }
                }

            } while (opcion != 0);

        }
    }
}
